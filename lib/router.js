//globals
Votes = new Mongo.Collection("votes");
userVotes = new Mongo.Collection("userVotes");
userSettings = new Mongo.Collection("settings");
Comments = new Mongo.Collection("comments");
userCommentsVotes = new Mongo.Collection("userCommentsVotes");

liveChannelPlaylists = new Mongo.Collection("liveChannelPlaylists");
liveChannelPlaylistLocation = new Mongo.Collection("liveChannelPlaylistLocation");

paginationAmount = 4;// min must be 3

Router.configure({
  layoutTemplate: 'appLayout'
});
Router.route('/', function () {

  if(Session.get('hoursToGoBack') == null) {
    Session.set('hoursToGoBack', 24);//default
  }
  if(Session.get("sortComments") == null) {
    Session.set("sortComments", 1);
  }
  var hoursToGoBack = Session.get('hoursToGoBack');
  dateHoursAgo = new Date();
  dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
  if(Session.get('skip') == null) {
    Session.set('skip', 0);
  }
  if(Session.get('pageNumber') == null) {
    var pageNumber = [0, null];
    Session.set('pageNumber',pageNumber);
   }
  this.subscribe('hotVotes', Session.get('skip'), paginationAmount, Session.get('hoursToGoBack')).wait();
  this.subscribe('userSettings').wait();
  this.subscribe('userVotes', Session.get('hoursToGoBack')).wait();

   if (this.ready()) {
    GAnalytics.pageview('/');
    Session.set('page', 'Hot');
    Session.set('mainVotesSize', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { weight: -1 }  }).count());
    if(Session.get('mainVotes') == null ) {
        Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { weight: -1 }, reactive: false}  ).fetch());
        Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { weight: -1 }, reactive: true}    ).fetch());
      }
    if(Session.get('mainVotesSize') > Session.get('mainVotes').length || Session.get('mainVotesSize') < Session.get('mainVotes').length) {
      //data change detected, must be changing days to sort
      var pageNumber = [0, null];
      Session.set('pageNumber',pageNumber);
      Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { weight: -1 }, reactive: false}  ).fetch());
      Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { weight: -1 }, reactive: true} ).fetch());
    }
    if(Session.get('mainVotes')[Session.get("pageNumber")[0]] !== undefined) {
      Meteor.subscribe('singleComments', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
      Meteor.subscribe('singleUserCommentsVotes', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
    }


    this.render('nav', {to: 'header'});
    this.render('mainContent', {to: 'content'});
    this.render('comments', {to: 'comments'});
   // this.render('footer', {to: 'footer'});
   } else {
    this.render('loading', {to: 'content'});
   }

    // this.render('footer', {to: 'footer'});

});
Router.route('/new', function () {

  if(Session.get('hoursToGoBack') == null) {
    Session.set('hoursToGoBack', 24);//default
  }
  if(Session.get("sortComments") == null) {
    Session.set("sortComments", 1);
  }
  var hoursToGoBack = Session.get('hoursToGoBack');
  dateHoursAgo = new Date();
  dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
  if(Session.get('skip') == null) {
    Session.set('skip', 0);
  }
  if(Session.get('pageNumber') == null) {
    var pageNumber = [0, null];
    Session.set('pageNumber',pageNumber);
   }
  this.subscribe('newVotes', Session.get('skip'), paginationAmount, Session.get('hoursToGoBack')).wait();
  this.subscribe('userSettings').wait();
  this.subscribe('userVotes', Session.get('hoursToGoBack')).wait();

   if (this.ready()) {
    // console.log(Votes.find().fetch());
    // console.log(userVotes.find().fetch());
    GAnalytics.pageview('/new');
     // console.log('dateHoursAgo: '+dateHoursAgo);
      Session.set('page', 'New');
      Session.set('mainVotesSize', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { createdAt: -1 }  }).count());
      if(Session.get('mainVotes') == null ) {
        Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { createdAt: -1 }, reactive: false}  ).fetch());
        Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { createdAt: -1 }, reactive: true}    ).fetch());
      }
      if(Session.get('mainVotesSize') > Session.get('mainVotes').length || Session.get('mainVotesSize') < Session.get('mainVotes').length) {
        //data change detected, must be changing days to sort
        var pageNumber = [0, null];
        Session.set('pageNumber',pageNumber);
        Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { createdAt: -1 }, reactive: false}  ).fetch());
        Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { createdAt: -1 }, reactive: true}    ).fetch());
      }

    Meteor.subscribe('singleComments', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
    Meteor.subscribe('singleUserCommentsVotes', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);


    this.render('nav', {to: 'header'});
    this.render('mainContent', {to: 'content'});
    this.render('comments', {to: 'comments'});
   // this.render('footer', {to: 'footer'});
   } else {
    this.render('loading', {to: 'content'});
   }

    // this.render('footer', {to: 'footer'});

});
Router.route('/top', function () {

  if(Session.get('hoursToGoBack') == null) {
    Session.set('hoursToGoBack', 24);//default
  }
  if(Session.get("sortComments") == null) {
    Session.set("sortComments", 1);
  }
  var hoursToGoBack = Session.get('hoursToGoBack');
  dateHoursAgo = new Date();
  dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);

  if(Session.get('skip') == null) {
    Session.set('skip', 0);
  }
  if(Session.get('pageNumber') == null) {
    var pageNumber = [0, null];
    Session.set('pageNumber',pageNumber);
   }
  // console.log("hours to go back: "+Session.get('hoursToGoBack'));
  this.subscribe('topVotes', Session.get('skip'), paginationAmount, Session.get('hoursToGoBack')).wait();
  this.subscribe('userSettings').wait();
  this.subscribe('userVotes', Session.get('hoursToGoBack')).wait();
   if (this.ready()) {
    GAnalytics.pageview('/top');
    //console.log('dateHoursAgo: '+dateHoursAgo);
      Session.set('page', 'Top');
    // console.log(Session.get('pageNumber')[0]);
    // console.log(Session.get('pageNumber')[1]);
    Session.set('mainVotesSize', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { upvote: -1, downvote: 1 }  }).count());

    if(Session.get('mainVotes') == null ) {
      Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { upvote: -1, downvote: 1 }, reactive: false}    ).fetch());
      Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { upvote: -1, downvote: 1 }, reactive: true}    ).fetch());
      //Session.set('timeTrigger', 0);
    }
    // else if(Session.get('timeTrigger')) {
    //   Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo } }, { sort: { upvote: -1, downvote: 1 }, reactive: false}    ).fetch());
    //   Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo } }, { sort: { upvote: -1, downvote: 1 }, reactive: true}    ).fetch());
    //   Session.set('timeTrigger', 0);
    // }
    // console.log(Session.get('mainVotes').length);
    // console.log(Session.get('mainVotesSize'));
    if(Session.get('mainVotesSize') > Session.get('mainVotes').length || Session.get('mainVotesSize') < Session.get('mainVotes').length) {
      //data change detected, must be changing days to sort
      var pageNumber = [0, null];
      Session.set('pageNumber',pageNumber);
      Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { upvote: -1, downvote: 1 }, reactive: false}    ).fetch());
      Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { upvote: -1, downvote: 1 }, reactive: true}    ).fetch());
    }
    // console.log(Session.get('mainVotes'));
    // console.log(Votes.find().fetch());
    Meteor.subscribe('singleComments', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
    Meteor.subscribe('singleUserCommentsVotes', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);


    this.render('nav', {to: 'header'});
    this.render('mainContent', {to: 'content'});
    this.render('comments', {to: 'comments'});
   // this.render('footer', {to: 'footer'});
   } else {
    this.render('loading', {to: 'content'});
   }

    // this.render('footer', {to: 'footer'});

});
Router.route('/submit', function () {

    if(Session.get('hoursToGoBack') == null) {
      Session.set('hoursToGoBack', 24);//default
    }
    var hoursToGoBack = Session.get('hoursToGoBack');
    dateHoursAgo = new Date();
    dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
    if(Session.get('skip') == null) {
      Session.set('skip', 0);
    }
    if(Session.get('pageNumber') == null) {
      var pageNumber = [0, null];
      Session.set('pageNumber',pageNumber);
     }
    this.subscribe('hotVotes', Session.get('skip'), paginationAmount, Session.get('hoursToGoBack')).wait();
    this.subscribe('userSettings').wait();
    this.subscribe('userVotes', Session.get('hoursToGoBack')).wait();
    if (this.ready()) {
      GAnalytics.pageview('/submit');
      this.render('nav', {to: 'header'});
      this.render('submitContent', {to: 'content'});
     } else {
      this.render('loading', {to: 'content'});
     }
    // this.render('footer', {to: 'footer'});
});
Router.route('/v/:_id', function () {

  if(Session.get('hoursToGoBack') == null) {
    Session.set('hoursToGoBack', 24);//default
  }
  if(Session.get("sortComments") == null) {
    Session.set("sortComments", 1);
  }
  var hoursToGoBack = Session.get('hoursToGoBack');
  dateHoursAgo = new Date();
  dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
  if(Session.get('skip') == null) {
    Session.set('skip', 0);
  }
  if(Session.get('pageNumber') == null) {
    var pageNumber = [0, null];
    Session.set('pageNumber',pageNumber);
   }
  this.subscribe('singleVotes', this.params._id).wait();
  this.subscribe('userSettings').wait();
  this.subscribe('singleUserVotes', this.params._id).wait();
  this.subscribe('singleComments', this.params._id).wait();
  this.subscribe('singleUserCommentsVotes', this.params._id).wait();
   if (this.ready()) {
    //console.log(Votes.find().fetch());
    GAnalytics.pageview('/v/'+this.params._id);
     // console.log('dateHoursAgo: '+dateHoursAgo);
      Session.set('page', 'Hot');
      Session.set('mainVotesSize', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }  }).count());
      if(Session.get('mainVotes') == null ) {
        Session.set('mainVotes', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: false}  ).fetch());
        Session.set('mainVotesReactive', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: true}    ).fetch());
      }
      if(Session.get('mainVotesSize') > Session.get('mainVotes').length || Session.get('mainVotesSize') < Session.get('mainVotes').length) {
        //data change detected, must be changing days to sort
        var pageNumber = [0, null];
        Session.set('pageNumber',pageNumber);
        Session.set('mainVotes', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: false}  ).fetch());
        Session.set('mainVotesReactive', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: true}    ).fetch());
      }
    this.render('nav', {to: 'header'});
    this.render('mainContent', {to: 'content'});
    this.render('comments', {to: 'comments'});
   // this.render('footer', {to: 'footer'});
   } else {
    this.render('loading', {to: 'content'});
   }
});
Router.route('/v/:_id/comments/:comment_id', function () {

  if(Session.get('hoursToGoBack') == null) {
    Session.set('hoursToGoBack', 24);//default
  }
  if(Session.get("sortComments") == null) {
    Session.set("sortComments", 1);
  }
  var hoursToGoBack = Session.get('hoursToGoBack');
  dateHoursAgo = new Date();
  dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
  if(Session.get('skip') == null) {
    Session.set('skip', 0);
  }
  if(Session.get('pageNumber') == null) {
    var pageNumber = [0, null];
    Session.set('pageNumber',pageNumber);
   }
  Session.set('specificRootComment_id', this.params.comment_id);
  this.subscribe('singleVotes', this.params._id).wait();
  this.subscribe('userSettings').wait();
  this.subscribe('singleUserVotes', this.params._id).wait();
  this.subscribe('singleComments', this.params._id).wait();
  this.subscribe('singleUserCommentsVotes', this.params._id).wait();
   if (this.ready()) {
    //console.log(Votes.find().fetch());
    GAnalytics.pageview('/v/'+this.params._id+'/comments/'+this.params.comment_id);
     // console.log('dateHoursAgo: '+dateHoursAgo);
      Session.set('page', 'Hot');
      Session.set('mainVotesSize', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }  }).count());
      if(Session.get('mainVotes') == null ) {
        Session.set('mainVotes', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: false}  ).fetch());
        Session.set('mainVotesReactive', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: true}    ).fetch());
      }
      if(Session.get('mainVotesSize') > Session.get('mainVotes').length || Session.get('mainVotesSize') < Session.get('mainVotes').length) {
        //data change detected, must be changing days to sort
        var pageNumber = [0, null];
        Session.set('pageNumber',pageNumber);
        Session.set('mainVotes', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: false}  ).fetch());
        Session.set('mainVotesReactive', Votes.find( { _id: this.params._id, deletedAt: null }, { sort: { createdAt: -1 }, reactive: true}    ).fetch());
      }
    this.render('nav', {to: 'header'});
    this.render('mainContent', {to: 'content'});
    this.render('comments', {to: 'comments'});
   // this.render('footer', {to: 'footer'});
   } else {
    this.render('loading', {to: 'content'});
   }
});
// Router.route('/channel/:user_id', function () {
//
//   if(Session.get('hoursToGoBack') == null) {
//     Session.set('hoursToGoBack', 24);//default
//   }
//   if(Session.get("sortComments") == null) {
//     Session.set("sortComments", 1);
//   }
//   var hoursToGoBack = Session.get('hoursToGoBack');
//   dateHoursAgo = new Date();
//   dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
//   if(Session.get('skip') == null) {
//     Session.set('skip', 0);
//   }
//   if(Session.get('pageNumber') == null) {
//     var pageNumber = [0, null];
//     Session.set('pageNumber',pageNumber);
//    }
//   this.subscribe('hotVotes', Session.get('skip'), paginationAmount, Session.get('hoursToGoBack')).wait();
//   this.subscribe('userSettings').wait();
//   this.subscribe('userVotes', Session.get('hoursToGoBack')).wait();
//
//    if (this.ready()) {
//     GAnalytics.pageview('/');
//     Session.set('page', 'Hot');
//     Session.set('mainVotesSize', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null, userid:this.params.user_id }, { sort: { weight: -1 }  }).count());
//     if(Session.get('mainVotes') == null ) {
//         Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null, userid:this.params.user_id }, { sort: { weight: -1 }, reactive: false}  ).fetch());
//         Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null, userid:this.params.user_id }, { sort: { weight: -1 }, reactive: true}    ).fetch());
//       }
//     if(Session.get('mainVotesSize') > Session.get('mainVotes').length || Session.get('mainVotesSize') < Session.get('mainVotes').length) {
//       //data change detected, must be changing days to sort
//       var pageNumber = [0, null];
//       Session.set('pageNumber',pageNumber);
//       Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null, userid:this.params.user_id  }, { sort: { weight: -1 }, reactive: false}  ).fetch());
//       Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null, userid:this.params.user_id }, { sort: { weight: -1 }, reactive: true} ).fetch());
//     }
//     if(Session.get('mainVotes')[Session.get("pageNumber")[0]] !== undefined) {
//       Meteor.subscribe('singleComments', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
//       Meteor.subscribe('singleUserCommentsVotes', Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
//     }
//
//
//     this.render('nav', {to: 'header'});
//     this.render('mainContent', {to: 'content'});
//     this.render('comments', {to: 'comments'});
//    // this.render('footer', {to: 'footer'});
//    } else {
//     this.render('loading', {to: 'content'});
//    }
// });
