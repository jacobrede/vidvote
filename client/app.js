if (Meteor.isClient) {
  //global time for info messages to dissapear
  timeout = 7000;
  globalFlag = false;//youtube state change fires multiple times, thus needs a 1 time use flag



  Accounts.ui.config({
    passwordSignupFields: "USERNAME_AND_EMAIL"
  });
  Accounts.onLogin(function(){
    //console.log("logging in");
    // if(userSettings.findOne() !== undefined) {
    //   $("#autoplay").html("AutoPlay: "+userSettings.findOne().autoPlay);
    //   $("#autonext").html("AutoNext: "+userSettings.findOne().autoNext);
    // }
    GAnalytics.event("account","signin");
  });

  accountsUIBootstrap3.logoutCallback = function(error) {
    //console.log("loggin out");
    GAnalytics.event("account","logout");
  }

  var next = function() {
   // console.log("next");
     var youtubeID = -1;
     // console.log("session page number + 1 = "+(Session.get("pageNumber")[0]+1));
     // console.log("length: "+Session.get('mainVotes').length);
     if((Session.get("pageNumber")[0]+1) == Session.get('mainVotes').length) {
       if(Session.get('hoursToGoBack') == 24) {
         $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">End of the current feed. You can change it above, or click here: <a href=\"#\" id=\"flashWeek\" class=\"alert-link\">Sort: 7 Days</a></div>");
         $('.alert').fadeOut(timeout);
       } else if(Session.get('hoursToGoBack') == 168) {
         $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">End of the current feed. You can change it above, or click here: <a href=\"#\" id=\"flashMonth\" class=\"alert-link\">Sort: 30 Days</a></div>");
         $('.alert').fadeOut(timeout);
       } else if(Session.get('hoursToGoBack') == 720) {
         $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">End of the current feed. You can change it above, or click here: <a href=\"#\" id=\"flashAll\" class=\"alert-link\">Sort: 9000 Days</a></div>");
         $('.alert').fadeOut(timeout);
       } else {
         $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">I dont know how you did it, but you managed to reach the end of everything. Heres a video: <a class=\"alert-link\" href=\"https://www.youtube.com/watch?v=1lbYHw-MHSo\">Hey</a></div>");
         $('.alert').fadeOut(timeout);
       }
       return;
     }

     try {
      youtubeID = Session.get('mainVotes')[Session.get("pageNumber")[0]+1].youtubeCode;//check to make sure that an id exists
     } catch(err) {
       $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">Something went wrong loading the next page.</div>");
       $('.alert').fadeOut(timeout);
       return;
     }
     Session.set('currentVideoId', youtubeID);
     $("#info").empty();
     Session.set("pageNumber",  [Session.get("pageNumber")[0]+1, youtubeID]);//actually increment the pagenumber

     if( (Session.get("pageNumber")[0] % (paginationAmount-1) == 0) && (Session.get('skip') < Session.get("pageNumber")[0]) ) {
      Session.set('skip', Session.get('skip')+paginationAmount);
     // Meteor.subscribe('Votes', Session.get('skip'), paginationAmount);
     }

      if(userSettings.findOne() !== undefined) {
       if(userSettings.findOne().autoPlay) {
        //console.log("loadvide");
           player.loadVideoById(youtubeID, 0, "large");
        } else {
           player.cueVideoById(youtubeID, 0, "large");
        }
      } else {
        //no user preferences here
        player.cueVideoById(youtubeID, 0, "large");
      }
      GAnalytics.event("nextClickPageNumber",Session.get("pageNumber")[0]);
  }
  var previous = function() {
    if(Session.get("pageNumber")[0] == 0) {
      //do nothing, since no previous
    } else {
      var youtubeID = Session.get('mainVotes')[Session.get("pageNumber")[0]-1].youtubeCode;
      Session.set('currentVideoId', youtubeID);
      $("#info").empty();
      Session.set("pageNumber", [ Session.get("pageNumber")[0]-1, youtubeID]);
      if(userSettings.findOne() !== undefined) {
        if(userSettings.findOne().autoPlay) {
          player.loadVideoById(youtubeID, 0, "large");
        } else {
          player.cueVideoById(youtubeID, 0, "large");
        }
      } else {
        //no user preferences here
        player.cueVideoById(youtubeID, 0, "large");
      }
      GAnalytics.event("previousClickPageNumber",Session.get("pageNumber")[0]);
    }
  }
  var upvote = function() {
    Meteor.call("upvote", Session.get('mainVotes')[Session.get("pageNumber")[0]]._id,function(error, result){
        if(error){
          $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
          $('.alert').fadeOut(timeout);
          return;
        } else {
          if(result) {
            $('#upvote').css('color', 'red');
          } else {
            $('#upvote').css('color', '');
          }
          GAnalytics.event("postUpvote",Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
          $("#info").empty();
          return;
        }
      });
    return;
  }
  var downvote = function() {
    Meteor.call("downvote", Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, function(error, result){
        if(error){
          $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
          $('.alert').fadeOut(timeout);
          return;
        } else {
         // success, TODO enforce only up or down vote / color
         if(result) {
           $('#downvote').css('color', 'blue');
         } else {
           $('#downvote').css('color', '');
         }
         GAnalytics.event("postDownvote",Session.get('mainVotes')[Session.get("pageNumber")[0]]._id);
         $("#info").empty();
          return;
        }
      });
    return;
  }
  var setCorrectPageSessionData = function(currentPage) {
    if(currentPage == "new") {
      Session.set("pageNumber", [0, null]);
      Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { createdAt: -1 }, reactive: false}  ).fetch());
      Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { createdAt: -1 }, reactive: true}    ).fetch());
    } else if (currentPage == "top") {
      Session.set("pageNumber", [0, null]);
      Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { upvote: -1, downvote: 1 }, reactive: false}    ).fetch());
      Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { upvote: -1, downvote: 1 }, reactive: true}    ).fetch());
    } else {
      Session.set("pageNumber", [0, null]);
      Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { weight: -1 }, reactive: false }).fetch());
      Session.set('mainVotesReactive', Votes.find( { createdAt: { $gte : dateHoursAgo }, deletedAt: null }, { sort: { weight: -1 }, reactive: true }).fetch());
    }
  }
  // Template._loginButtonsAdditionalLoggedInDropdownActions.events({
  //   'click #channel': function() {
  //     Router.go('/channel/'+Meteor.user().username);
  //   }
  // });

  Template.nav.events({
    'click #autoplay': function() {
      if(! Meteor.userId()) {
        $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">Error: you must log in to use that feature!</div>");
        $('.alert').fadeOut(timeout);
        return;
      }
        if(userSettings.findOne().autoPlay == 0) {
          Meteor.call("updateAutoPlay", function(error, result){
            if(error){
              $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
              $('.alert').fadeOut(timeout);
              return;
            } else {
               //no need to update, success
              return;
            }
          });
        } else {
          Meteor.call("updateAutoPlay", function(error, result){
            if(error){
              $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
              $('.alert').fadeOut(timeout);
              return;
            } else {
               //no need to update, success
              return;
            }
          });
        }
      },
      'click #autonext': function() {
      if(! Meteor.userId()) {
        $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">Error: you must log in to use that feature!</div>");
        $('.alert').fadeOut(timeout);
        return;
      }
        if(userSettings.findOne().autoNext == 0) {
          Meteor.call("updateAutoNext",function(error, result){
            if(error){
              $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
              $('.alert').fadeOut(timeout);
              return;
            } else {
               //no need to update, success
              return;
            }
          });
        } else {
          Meteor.call("updateAutoNext",function(error, result){
            if(error){
              $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
              $('.alert').fadeOut(timeout);
              return;
            } else {
               //no need to update, success
              return;
            }
          });
        }
      },
    'click #day': function() {
      Session.set('hoursToGoBack', 24);
      setCorrectPageSessionData(Session.get('page').toLowerCase());
    },
    'click #week': function() {
      Session.set('hoursToGoBack', 168);
      setCorrectPageSessionData(Session.get('page').toLowerCase());
    },
    'click #month': function() {
      Session.set('hoursToGoBack', 720);
      setCorrectPageSessionData(Session.get('page').toLowerCase());
    },
    'click #all': function() {
      Session.set('hoursToGoBack', 216000);
      setCorrectPageSessionData(Session.get('page').toLowerCase());
    },
    'click #upvote': function(){
      upvote();
    },
    'click #downvote': function(){
      downvote();
    },
    'click #next': function() {
       next();
    },
    'click #previous': function() {
      previous();
    },
    'click #New': function() {
      Router.go('/new');
      Session.set('page', 'New');
      setCorrectPageSessionData("new");
    },
    'click #Hot': function() {
      Router.go('/');
      Session.set('page', 'Hot');
      setCorrectPageSessionData("");
    },
    'click #Top': function() {
      Router.go('/top');
      Session.set('page', 'Top');
      setCorrectPageSessionData("top");
    },

   });
  Template.nav.helpers({
    hasUpVoted: function() {
      if(userVotes.findOne({ urlID: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id}) === undefined) {
        return 0;
      }
      return userVotes.findOne({ urlID: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id}).upvoted;
    },
    hasDownVoted: function() {
      if(userVotes.findOne({ urlID: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id}) === undefined) {
        return 0;
      }
      return userVotes.findOne({ urlID: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id}).downvoted;
    },
    numUpVotes: function() {
      if(Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }) === undefined) {
        return "?";
      }
      return Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }).upvote;
    },
    numDownVotes: function() {
      if(Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }) === undefined) {
        return "?";
      }
      return Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }).downvote;
    },
    getWeight: function() {
      if(Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }) === undefined) {
        return "?";
      }
      return Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }).weight;
    },
    getScore: function() {
      if(Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }) === undefined) {
        return "?";
      }
      return (Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }).upvote -
      Votes.findOne({ youtubeCode: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode }).downvote);
    },
    getPage: function() {
      return Session.get('page');
    },
    getDateToGoBack: function() {
      if(Session.get('hoursToGoBack') == null) {
        return 1;
      }
      var days = Session.get('hoursToGoBack') / 24;
      return days;
    },
    getAutoPlay: function() {
      if(userSettings.findOne() === undefined) {
        return "?";
      }
      return userSettings.findOne().autoPlay;
    },
    getAutoNext: function() {
      if(userSettings.findOne() === undefined) {
        return "?";
      }
      return userSettings.findOne().autoNext;
    }
  });
  Template.nav.onRendered(function () {

  });
  Template.nav.onCreated(function () {
   // console.log("nav created");

  });

  Template.body.events({
    'keydown': function(e) {
      // if(e.keyCode == 32) {
      //   console.log(document.getElementsByClassName("comment"));
      //     if(document.getElementsByClassName("comment") !== document.activeElement) {
      //        e.preventDefault();
      //       return false;
      //     }
      //   }
    },
    'keyup': function(e) {
      //console.log(e.keyCode);
      // if(e.keyCode == 32) {
      //   var allComments = document.getElementsByClassName("comment") ;
      //   allComments.foreach(function(entry) {
      //     if(entry !== document.activeElement) {

      //     }
      //   });
      //   if(document.getElementsByClassName("comment") !== document.activeElement) {
      //     if(player.getPlayerState() == 1) {//is playing
      //       player.pauseVideo();
      //     } else {
      //       player.playVideo();
      //     }
      //   }
      // } else
       if(e.keyCode == 39) {//next
        next();
      } else if(e.keyCode == 37) {//previous
        previous();
      } else if (e.keyCode == 38) {
        upvote();
      } else if (e.keyCode == 40) {
        downvote();
      }
    },
    'click #flashWeek': function() {
      Session.set('hoursToGoBack', 168);
      setCorrectPageSessionData(Session.get('page').toLowerCase());
    },
    'click #flashMonth': function() {
      Session.set('hoursToGoBack', 720);
      setCorrectPageSessionData(Session.get('page').toLowerCase());
    },
    'click #flashAll': function() {
      Session.set('hoursToGoBack', 216000);
      setCorrectPageSessionData(Session.get('page').toLowerCase());
    }
  });

Template.notification.events({
  'click #flashWeek': function() {
    Session.set('hoursToGoBack', 168);
    setCorrectPageSessionData(Session.get('page').toLowerCase());
  },
  'click #flashMonth': function() {
    Session.set('hoursToGoBack', 720);
    setCorrectPageSessionData(Session.get('page').toLowerCase());
  },
  'click #flashAll': function() {
    console.log("inside");
    Session.set('hoursToGoBack', 216000);
    setCorrectPageSessionData(Session.get('page').toLowerCase());
  }
});


  Template.mainContent.events({
    'click #makePlayerSmaller': function() {
      if(Session.get('playerSizeState') === undefined) {
        Session.set('playerSizeState', 0);//0 = no state, large video. 1 = state change, smaller video
      }
      if(!Session.get('playerSizeState')) {
          $('#playerWrapper').addClass('col-md-10 col-md-offset-1');
          Session.set('playerSizeState', 1);
      } else {
        $('#playerWrapper').removeClass('col-md-10 col-md-offset-1');
        Session.set('playerSizeState', 0);
      }

    }
  });
  Template.mainContent.helpers({
    isVideoSizeSet: function() {
      if(Session.get('playerSizeState') === undefined) {
        return false;
      }
      return Session.get('playerSizeState') == 1;
    },
    getVideoSize: function() {
      if(Session.get('playerSizeState') == 1) {
        return 'col-md-10 col-md-offset-1';
      } else {
        return '';
      }
    },
    resizeButtonText: function() {
      if(Session.get('playerSizeState') == 1) {
        return 'Resize Video Larger';
      } else {
        return 'Resize Video Smaller';
      }
    }
  });
  Template.mainContent.onCreated(function(){

      // YouTube API will call onYouTubeIframeAPIReady() when API ready.
      // Make sure it's a global variable.
      onYouTubeIframeAPIReady = function () {
          // New Video Player, the first argument is the id of the div.
          // Make sure it's a global variable.
          player = new YT.Player("player", {
              height: "100%",
              width: "100%",
              // videoId is the "v" in URL (ex: http://www.youtube.com/watch?v=LdH1hSWGFGU, videoId = "LdH1hSWGFGU")
              videoId: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode,
              // Events like ready, state change,
              events: {
                  onReady: function (event) {

                      if(userSettings.findOne() !== undefined) {
                        if(userSettings.findOne().autoPlay) {
                          // Play video when player ready.
                          event.target.playVideo();
                          var youtubeID = Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode;
                          Session.set('currentVideoId', youtubeID);
                        }
                      }
                  },
                  onStateChange: function(event) {
                    if(userSettings.findOne() !== undefined) {
                      if(userSettings.findOne().autoNext) {
                        if(event.data === 0) { //make sure video has ended
                          if(!globalFlag) {
                            globalFlag = true;
                            next();
                          }
                        } else if(event.data === 1) {
                          globalFlag = false;
                        }
                      }
                    }
                  },
                  onError: function(event) {
                    //console.log(event.data);
                    //need to specifcy deltedat value on the client as well.
                     if(event.data === 100 || event.data === 101 || event.data === 150) {
                        Meteor.call("deleteURL", event.data, Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, function(error, result) {
                          if(error){
                            $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
                            $('.alert').fadeOut(timeout);
                            return;
                          } else {
                            $("#info").empty();
                            $("#info").html("<div class=\"alert alert-info\" role=\"alert\">Info: Reset stream, removed video that would not display. Everything has been updated and works as intended now.</div>");
                            previous();
                            $('.alert').fadeOut(timeout);
                            return;
                          }
                        });
                     }
                  }
              }
          });
      };
      YT.load();
  });
  Template.submitContent.events({
      'submit form': function(event, template){
        event.preventDefault();

        var url = event.target.url.value;
        // Insert a task into the collection
        Meteor.call("addURL", url, function(error, result){
          if(error){
            $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
            $('.alert').fadeOut(timeout);
            return;
          } else {
            $("#info").empty();
            //console.log('inside return success');
            //Meteor.subscribe('newVotes', Session.get('skip'), paginationAmount, Session.get('hoursToGoBack'));
           // Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo } }, { sort: { createdAt: -1 } }).fetch());
            Router.go('/new');
            return;
          }
        });
    }
  });
  Template.comments.events({
      'submit form': function(event, template){
        event.preventDefault();

        var commentText = $.trim(event.target.comment.value);
        if(commentText.length <= 0) {
          $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">Error: Cannot comment nothing</div>");
          $('.alert').fadeOut(timeout);
          return;
        }
        var post_id = event.target.post_id.value;
       // console.log(post_id);
        var parent_id = event.target.parent_id.value;
       // console.log(parent_id);
        // Insert a task into the collection
        Meteor.call("addComment", commentText, post_id, parent_id, function(error, result){
          if(error){
            $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
            $('.alert').fadeOut(timeout);
            return;
          } else {
            $("#info").empty();
             $("#info").html("<div class=\"alert alert-success\" role=\"alert\">Comment Successfully Added!</div>");
             $('#commentToggle_'+parent_id).click();
             $('.alert').fadeOut(timeout);
            return;
          }
        });
     },
     'click .commentUpvote': function(event) {
      var commentID = $(event.currentTarget).data('id');
      var postID = Session.get('mainVotes')[Session.get("pageNumber")[0]]._id;
      //console.log("commentID: "+commentID);
      Meteor.call("commentUpvote", commentID, postID, function(error, result){
          if(error){
            $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
            $('.alert').fadeOut(timeout);
            return;
          } else {
            $("#info").empty();
            return;
          }
        });
    },
    'click .commentDownvote': function(event) {
      var commentID = $(event.currentTarget).data('id');
      var postID = Session.get('mainVotes')[Session.get("pageNumber")[0]]._id;
        Meteor.call("commentDownvote", commentID, postID, function(error, result){
          if(error){
            $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
            $('.alert').fadeOut(timeout);
            return;
          } else {
            $("#info").empty();
            return;
          }
        });
    },
    'click #sortTopComments': function() {
      Session.set("sortComments", 1);
    },
    'click #sortNewComments': function() {
      Session.set("sortComments", 2);
    },
    'click #expando': function(event) {
      var commentID = $(event.currentTarget).data('id');
      //console.log($(event.currentTarget).children().hasClass('glyphicon-minus'));
      if($(event.currentTarget).children().hasClass('glyphicon-minus')) {
        $(event.currentTarget).children().removeClass('glyphicon-minus').addClass('glyphicon-plus');
      } else {
        $(event.currentTarget).children().removeClass('glyphicon-plus').addClass('glyphicon-minus');
      }
    },
    'click #denyDelete': function(event) {
      var commentID = $(event.currentTarget).data('id');
      $('#deleteCommentToggle_'+commentID).click();
    },
    'click #confirmDelete': function(event) {
      var commentID = $(event.currentTarget).data('id');
      if(Meteor.userId() != Comments.findOne({ "auth.id": Meteor.userId(), post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: commentID}).auth.id) {
        $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">[Error: You cannot delete this comment since you are not the author.]</div>");
        $('#deleteCommentToggle_'+commentID).click();
        $('.alert').fadeOut(timeout);
        return;
      } else {
        Meteor.call("deleteComment", commentID, function(error, result){
          if(error){
            $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
            $('#deleteCommentToggle_'+commentID).click();
            $('.alert').fadeOut(timeout);
            return;
          } else {
            $("#info").empty();
            $('#deleteCommentToggle_'+commentID).click();
            return;
          }
        });
      }

    },
    'click #shareComment': function(event) {
      var specificComment_id = $(event.currentTarget).data('id');
      var post_id = $(event.currentTarget).data('post_id');
      Session.set('specificRootComment_id', specificComment_id);
      Router.go('/v/'+post_id+'/comments/'+specificComment_id);
    },
    'click #loadSpecificCommentThread': function(event) {
      var specificComment_id = $(event.currentTarget).data('id');
      var post_id = $(event.currentTarget).data('post_id');
      Session.set('specificRootComment_id', specificComment_id);
      Router.go('/v/'+post_id+'/comments/'+specificComment_id);
    },
    'click #loadAllComments': function(event) {
      var endingURL = $(event.currentTarget).data('id');
      Router.go('/v/'+endingURL);
      Session.set('specificRootComment_id', null);
    }
  });
  Template.comments.helpers({
    pageID: function() {
      return Session.get('mainVotes')[Session.get("pageNumber")[0]]._id;
    },
    rootComments: function() {
      //console.log(Comments.find({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id}).fetch());
      if(Session.get("sortComments") == 1) {//top votes
        if(Session.get('specificRootComment_id') == null) {
          return Comments.find(
          { post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id },
          { sort: { upvote: -1, downvote: 1 } });
        } else {
          //specific comment id is set, load only that id as a root
          return Comments.find(
          { post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: Session.get('specificRootComment_id') },
          { sort: { upvote: -1, downvote: 1 } });
        }
      } else {
        if(Session.get('specificRootComment_id') == null) {
          return Comments.find(
          { post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id },
          { sort: { createdAt: -1 } });
        } else {
          //specific comment id is set, load only that id as a root
          return Comments.find(
          { post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: Session.get('specificRootComment_id') },
          { sort: { createdAt: -1 } });
        }
      }
    },
    numberComments: function() {
      return Comments.find({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id}).count();
    },
    hasSpecificRootComment: function() {
      return Session.get('specificRootComment_id') != null;
    }
  });

  Template.comment.helpers({
    justToDeep: function(parentID) {
      if(Session.get('specificRootComment_id') == null) {
        if(Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID})) {
          var counter = 0;
            var parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID}).parent_id;
            while(parentBackwards_id != Session.get('mainVotes')[Session.get("pageNumber")[0]]._id) {
              parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}).parent_id;
              counter++;
            }
            if(counter > 4) {
              return true;
            } else {
              return false;
            }
        }
      } else {
          if(Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID})) {
            var counter = 0;
              var parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID}).parent_id;
              while(parentBackwards_id != Session.get('specificRootComment_id')) {
                if(Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}) === undefined) {
                  break;
                }
                parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}).parent_id;
                counter++;
              }
              if(counter > 4) {
                return true;
              } else {
                return false;
              }
          }
      }
      return false;
    },
    hasChildren: function(parentID) {
      if(Session.get('specificRootComment_id') == null) {
        if(Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID})) {
          var counter = 0;
            var parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID}).parent_id;
            while(parentBackwards_id != Session.get('mainVotes')[Session.get("pageNumber")[0]]._id) {
              parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}).parent_id;
              //console.log("parent backwards message: "+Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}).text);
              counter++;
            }
          if(counter > 4) {
            return false;
          } else {
            return true;
          }
        }
      } else {
        if(Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID})) {
          var counter = 0;
            var parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID}).parent_id;
            while(parentBackwards_id != Session.get('specificRootComment_id')) {
              if(Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}) === undefined) {
                break;
              }
              parentBackwards_id = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}).parent_id;
              //console.log("parent backwards message: "+Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: parentBackwards_id}).text);
              counter++;
            }
          if(counter > 4) {
            return false;
          } else {
            return true;
          }
        }
      }
      return false;
    },
    childComments: function(parentID) {
      //console.log("parent recieved: "+parentID);

      if(Session.get("sortComments") == 1) {//top votes
        return Comments.find(
        {post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID },
        { sort: { upvote: -1, downvote: 1 } });
      } else {
        return Comments.find(
        {post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, parent_id: parentID },
        { sort: { createdAt: -1 } });
      }
    },
    isOP: function(userID) {
      if(Session.get('mainVotes')[Session.get("pageNumber")[0]].userId == userID) {
        return true;
      }
      return false;
    },
    isDeletedComment: function(commentID) {
      var deletedAtTimeStamp = Comments.findOne({post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: commentID}).deletedAt;
      if(deletedAtTimeStamp === null) {
        return false;
      }
      return true;
    },
    hasUpVotedComment: function(commentID) {
      if(userCommentsVotes.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, comment_id: commentID, userID: Meteor.userId()}) === undefined) {
        return 0;
      }
      //console.log(userCommentsVotes.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, comment_id: commentID, userID: Meteor.userId}));
      return userCommentsVotes.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, comment_id: commentID, userID: Meteor.userId()}).upvoted;
    },
    hasDownVotedComment: function(commentID) {
      if(userCommentsVotes.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, comment_id: commentID, userID: Meteor.userId()}) === undefined) {
        return 0;
      }
      return userCommentsVotes.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, comment_id: commentID, userID: Meteor.userId()}).downvoted;
    },
    getTotalCommentScore: function(commentID) {
      if(Comments.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: commentID}) === undefined) {
        return 0;
      }
      return Comments.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: commentID}).upvote -
      Comments.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: commentID}).downvote;
    },
    isOriginalCommentPoster: function(commentID) {
      if(Comments.findOne({ post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: commentID}) === undefined) {
        return 0;
      }
      if(Comments.findOne({ "auth.id": Meteor.userId(), post_id: Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, _id: commentID})) {
        return 1;
      }
      return 0;
    }
  });
  // Template.comment.events({
  //   'click #expando': function(event) {
  //     var commentID = $(event.currentTarget).data('id');
  //    console.log($(event.currentTarget).hasClass('glyphicon-minus'));
  //     if($(event.currentTarget).hasClass('glyphicon-minus')) {
  //       $(event.currentTarget).removeClass('glyphicon-minus').addClass('glyphicon-plus');
  //     } else {
  //       $(event.currentTarget).removeClass('glyphicon-plus').addClass('glyphicon-minus');
  //     }

  //   }

  // });
}
