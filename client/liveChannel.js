Template.liveChannel.events({
  'click #makePlayerSmaller': function() {
    if(Session.get('playerSizeState') === undefined) {
      Session.set('playerSizeState', 0);//0 = no state, large video. 1 = state change, smaller video
    }
    if(!Session.get('playerSizeState')) {
        $('#playerWrapper').addClass('col-md-10 col-md-offset-1');
        Session.set('playerSizeState', 1);
    } else {
      $('#playerWrapper').removeClass('col-md-10 col-md-offset-1');
      Session.set('playerSizeState', 0);
    }

  },
  'click #goLive': function() {
    Meteor.call("goLive",function(error, result){
      if(error){
        $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
        $('.alert').fadeOut(timeout);
        return;
      } else {
        $("#info").html("<div class=\"alert alert-success\" role=\"alert\">"+result+"</div>");
        $('.alert').fadeOut(timeout);
        return;
      }
    });
  }
});
Template.liveChannel.helpers({
  isVideoSizeSet: function() {
    if(Session.get('playerSizeState') === undefined) {
      return false;
    }
    return Session.get('playerSizeState') == 1;
  },
  getVideoSize: function() {
    if(Session.get('playerSizeState') == 1) {
      return 'col-md-10 col-md-offset-1';
    } else {
      return '';
    }
  },
  resizeButtonText: function() {
    if(Session.get('playerSizeState') == 1) {
      return 'Resize Video Larger';
    } else {
      return 'Resize Video Smaller';
    }
  }
});
Template.liveChannel.onCreated(function(){

    // YouTube API will call onYouTubeIframeAPIReady() when API ready.
    // Make sure it's a global variable.
    onYouTubeIframeAPIReady = function () {
        // New Video Player, the first argument is the id of the div.
        // Make sure it's a global variable.
        player = new YT.Player("player", {
            height: "100%",
            width: "100%",
            // videoId is the "v" in URL (ex: http://www.youtube.com/watch?v=LdH1hSWGFGU, videoId = "LdH1hSWGFGU")
            videoId: Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode,
            // Events like ready, state change,
            events: {
                onReady: function (event) {

                    if(userSettings.findOne() !== undefined) {
                      if(userSettings.findOne().autoPlay) {
                        // Play video when player ready.
                        event.target.playVideo();
                        var youtubeID = Session.get('mainVotes')[Session.get("pageNumber")[0]].youtubeCode;
                        Session.set('currentVideoId', youtubeID);
                      }
                    }
                },
                onStateChange: function(event) {
                  if(userSettings.findOne() !== undefined) {
                    if(userSettings.findOne().autoNext) {
                      if(event.data === 0) { //make sure video has ended
                        if(!globalFlag) {
                          globalFlag = true;
                          next();
                        }
                      } else if(event.data === 1) {
                        globalFlag = false;
                      }
                    }
                  }
                },
                onError: function(event) {
                  //console.log(event.data);
                  //need to specifcy deltedat value on the client as well.
                   if(event.data === 100 || event.data === 101 || event.data === 150) {
                      Meteor.call("deleteURL", event.data, Session.get('mainVotes')[Session.get("pageNumber")[0]]._id, function(error, result) {
                        if(error){
                          $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
                          $('.alert').fadeOut(timeout);
                          return;
                        } else {
                          $("#info").empty();
                          $("#info").html("<div class=\"alert alert-info\" role=\"alert\">Info: Reset stream, removed video that would not display. Everything has been updated and works as intended now.</div>");
                          previous();
                          $('.alert').fadeOut(timeout);
                          return;
                        }
                      });
                   }
                }
            }
        });
    };
    YT.load();
});
Template.submitLiveChannel.events({
  'submit form': function(event, template){
    event.preventDefault();

    var url = event.target.url.value;
    var channel = event.target.channel.value;
    // Insert a task into the collection
    Meteor.call("addLiveChannelURL", url, channel, function(error, result){
      if(error){
        $("#info").html("<div class=\"alert alert-danger\" role=\"alert\">"+error.message+"</div>");
        $('.alert').fadeOut(timeout);
        return;
      } else {
        $("#info").empty();
        //console.log('inside return success');
        //Meteor.subscribe('newVotes', Session.get('skip'), paginationAmount, Session.get('hoursToGoBack'));
       // Session.set('mainVotes', Votes.find( { createdAt: { $gte : dateHoursAgo } }, { sort: { createdAt: -1 } }).fetch());
        return;
      }
    });
  }
});
Template.submitLiveChannel.helpers({
  currentChannel: function() {
    return Session.get('currentChannel');
  }
});
Template.liveChannelPlaylist.helpers({
  playlistData: function() {
    return liveChannelPlaylists.find({channel_username: Session.get('currentChannel')});
  }
});
