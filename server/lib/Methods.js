var archiveHoursAmount = 720;//168 = 7 days

Meteor.methods({
  addURL: function (text) {
    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    var validYoutubeRegex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    if(text.match(validYoutubeRegex)) {
      var youtubeID = RegExp.$1;
    } else {
       throw new Meteor.Error("Error: Not a valid Youtube URL");
    }
    var hoursToGoBack = 504;//21 days
    var dateHoursAgo = new Date();
    dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
    //console.log("dateHoursAgo: "+dateHoursAgo);
    if(Votes.findOne({ createdAt: { $gte: dateHoursAgo }, youtubeCode: { $eq: youtubeID }  }) !== undefined) {
      throw new Meteor.Error("Error: Link has already been submitted recently.");
    }
    var createdDate = new Date();

    Votes.insert({
      youtubeCode: youtubeID,
      upvote: 0,
      downvote: 0,
      createdAt: createdDate,
      weight: 0,
      deletedAt: null,
      userId: Meteor.userId()
    });
    return 1;
  },
  deleteURL: function(videoErrorCode, vote_id) {
    //console.log(videoErrorCode);
    //make sure a valid error code has been submitted, since this is setting deletedAt
    if(!Votes.findOne({ _id: vote_id})) {
      throw new Meteor.Error("Error: That post doesnt exist.");
    }
    if(videoErrorCode === 100 || videoErrorCode === 101 || videoErrorCode === 150) {
      Votes.update(
        { _id: vote_id },
        { $set: { deletedAt: new Date()} }
      );
    } else {
      throw new Meteor.Error("Error: Invalid operation detected.");
    }
  },
  addComment: function (commentText, post_id, parent_id) {
    // Make sure the user is logged in before inserting a task
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
   // console.log(post_id);
    if(!Votes.findOne({ _id: post_id})) {
      throw new Meteor.Error("Error: That post doesnt exist.");
    }
    if(post_id != parent_id) {//parent must exist in comments
      if(!Comments.findOne({_id: parent_id})) {
        throw new Meteor.Error("Error: No parent comment found.");
      }
    }
    if(commentText.length <= 0) {
      throw new Meteor.Error("Error: Cannot comment nothing.");
    }
    commentText = UniHTML.purify(commentText);
    var secondsToGoBack = 10;
    var dateSecondsAgo = new Date();
    dateSecondsAgo.setSeconds(dateSecondsAgo.getSeconds()-secondsToGoBack);
    if(Comments.findOne({ "auth.id": Meteor.userId(), "post_id": post_id},{ sort: { createdAt: -1 } }) !== undefined) {
      //console.log(Comments.findOne({ "auth.id": Meteor.userId(), "post_id": post_id},{ sort: { createdAt: -1 } }).createdAt);
      var dateCreated = new Date(Comments.findOne({ "auth.id": Meteor.userId(), "post_id": post_id},{ sort: { createdAt: -1 } }).createdAt);
      if(dateCreated.getTime() > dateSecondsAgo.getTime()) {
          var dif = dateSecondsAgo.getTime() - dateCreated.getTime();
          var secondsLeft = Math.round(dif / 1000, 2);
          secondsLeft = Math.abs(secondsLeft);
          throw new Meteor.Error("Error: You must wait '"+secondsLeft+"' more seconds to Comment again.");
      }
    }
    Comments.insert({
      post_id: post_id,
      parent_id: parent_id,
      createdAt: new Date(),
      auth: { id: Meteor.userId(), name: Meteor.user().username},
      text: commentText,
      deletedAt: null,
      upvote: 0,
      downvote: 0
    });
    // if(Comments.findOne({ createdAt: { $gte: dateHoursAgo }, youtubeCode: { $eq: youtubeID }  }) !== undefined) {
    //   throw new Meteor.Error("Error: Link has already been submitted recently.");
    // }
  },
  updateAutoPlay: function() {
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(userSettings.findOne({userID: Meteor.userId()}).autoPlay == 0) {
      userSettings.update(
        { userID: Meteor.userId() },
        { $set: { autoPlay: 1}
      });
    } else {
      userSettings.update(
        { userID: Meteor.userId() },
        { $set: { autoPlay: 0} }
      );
    }
  },
  updateAutoNext: function() {
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(userSettings.findOne({userID: Meteor.userId()}).autoNext == 0) {
      userSettings.update(
        { userID: Meteor.userId() },
        { $set: { autoNext: 1} }
        );
    } else {
      userSettings.update(
        { userID: Meteor.userId() },
        { $set: { autoNext: 0} }
        );
    }
  },
  upvote: function(currentURLID) {

    // console.log("current: "+(new Date()));
    // console.log("date: "+dateHoursAgo);
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(Votes.findOne({_id: currentURLID})) {
      var daysToArchive = new Date();
      daysToArchive.setHours(daysToArchive.getHours()-archiveHoursAmount);
      var dateCreatedAt = new Date(Votes.findOne({_id: currentURLID}).createdAt);
      // console.log("one day ago: "+daysToArchive);
      // console.log("created: "+dateCreatedAt);
      if(dateCreatedAt.getTime() < daysToArchive.getTime()) {
        throw new Meteor.Error("Error: This post has been archived.");
      }
    }
    if(userVotes.findOne({userID: Meteor.userId(), urlID: currentURLID})) {
      var userVoteInfo = userVotes.findOne({userID: Meteor.userId(), urlID: currentURLID});
      var secondsToGoBack = 3;
      var dateSecondsAgo = new Date();
      dateSecondsAgo.setSeconds(dateSecondsAgo.getSeconds()-secondsToGoBack);
      var dateUpdated = new Date(userVoteInfo.updatedAt);
      // console.log("current: "+(new Date()));
      // console.log("updated: "+dateUpdated);
      // console.log("dateSec: "+dateSecondsAgo);
      if(dateUpdated.getTime() > dateSecondsAgo.getTime()) {
      	//console.log("inside: ");
      	var dif = dateSecondsAgo.getTime() - dateUpdated.getTime();
      	var secondsLeft = Math.round(dif / 1000, 2);
      	secondsLeft = Math.abs(secondsLeft);
		    throw new Meteor.Error("Error: You must wait '"+secondsLeft+"' more seconds to vote.");
      }
      if(userVoteInfo.upvoted) {
       // throw new Meteor.Error("Error: Allready upvoted.");
       //console.log("allready upvoted");
        userVotes.update(
         { userID: Meteor.userId(), urlID: currentURLID },
         { $set: { upvoted: 0, downvoted: 0, updatedAt: new Date()} }
        );
        Votes.update(
          { _id: currentURLID },
          { $inc: { upvote: -1} }
        );
        return 0;//turn color off
      } else if(userVoteInfo.downvoted) {
      	//console.log("allready downvoted");
        userVotes.update(
         { userID: Meteor.userId(), urlID: currentURLID },
         { $set: { upvoted: 1, downvoted: 0, updatedAt: new Date()} }
        );
        Votes.update(
          { _id: currentURLID },
          { $inc: { downvote: -1, upvote: 1} }
        );
        return 1;//turn color on
      } else if(!userVoteInfo.downvoted && !userVoteInfo.upvoted) {
      	//console.log("not down or up");
      	userVotes.update(
         { userID: Meteor.userId(), urlID: currentURLID },
         { $set: { upvoted: 1, downvoted: 0, updatedAt: new Date()} }
        );
        Votes.update(
          { _id: currentURLID },
          { $inc: { upvote: 1} }
        );
        return 1;//turn color on
      } else {
        throw new Meteor.Error("Error: Something wrong with user voting.");
      }
    } else {
      //no userVotes in database, must create
       userVotes.insert({
          userID: Meteor.userId(),
          urlID: currentURLID,
          upvoted: 1,
          downvoted: 0,
          createdAt: new Date(),
          updatedAt: new Date()
        });
       Votes.update(
          { _id: currentURLID },
          { $inc: { upvote: 1} }
        );
       return 1;//turn color on
    }
  },
  downvote: function(currentURLID) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(Votes.findOne({_id: currentURLID})) {
      var daysToArchive = new Date();
      daysToArchive.setHours(daysToArchive.getHours()-archiveHoursAmount);
      var dateCreatedAt = new Date(Votes.findOne({_id: currentURLID}).createdAt);
      // console.log("one day ago: "+daysToArchive);
      // console.log("created: "+dateCreatedAt);
      if(dateCreatedAt.getTime() < daysToArchive.getTime()) {
        throw new Meteor.Error("Error: This post has been archived.");
      }
    }
    if(userVotes.findOne({userID: Meteor.userId(), urlID: currentURLID})) {
      var userVoteInfo = userVotes.findOne({userID: Meteor.userId(), urlID: currentURLID});
      //console.log(userVoteInfo);
      var secondsToGoBack = 3;
      var dateSecondsAgo = new Date();
      dateSecondsAgo.setSeconds(dateSecondsAgo.getSeconds()-secondsToGoBack);
      var dateUpdated = new Date(userVoteInfo.updatedAt);
      // console.log("current: "+(new Date()));
      // console.log("updated: "+dateUpdated);
      // console.log("dateSec: "+dateSecondsAgo);
      if(dateUpdated.getTime() > dateSecondsAgo.getTime()) {
      	//console.log("inside: ");
      	var dif = dateSecondsAgo.getTime() - dateUpdated.getTime();
      	var secondsLeft = Math.round(dif / 1000, 2);
      	secondsLeft = Math.abs(secondsLeft);
		    throw new Meteor.Error("Error: You must wait '"+secondsLeft+"' more seconds to vote.");
      }
      if(userVoteInfo.downvoted) {
      	console.log("allready downvoted");
       // throw new Meteor.Error("Error: Allready downvoted.");
        userVotes.update(
         { userID: Meteor.userId(), urlID: currentURLID },
         { $set: { upvoted: 0, downvoted: 0, updatedAt: new Date()} }
        );
        Votes.update(
          { _id: currentURLID },
          { $inc: { downvote: -1} }
        );
        return 0;//turn color off
      } else if(userVoteInfo.upvoted) {
      	console.log("allready upvoted");
        userVotes.update(
         { userID: Meteor.userId(), urlID: currentURLID },
         { $set: { upvoted: 0, downvoted: 1, updatedAt: new Date()} }
        );
        Votes.update(
          { _id: currentURLID },
          { $inc: { downvote: 1, upvote: -1} }
        );
        return 1;//turn color on
      } else if(!userVoteInfo.downvoted && !userVoteInfo.upvoted) {
      	console.log("not down or up");
      	userVotes.update(
         { userID: Meteor.userId(), urlID: currentURLID },
         { $set: { upvoted: 0, downvoted: 1, updatedAt: new Date()} }
        );
        Votes.update(
          { _id: currentURLID },
          { $inc: { downvote: 1} }
        );
        return 1;//turn color on
      } else {
        throw new Meteor.Error("Error: Something wrong with user voting.");
      }
    } else {
      //no userVotes in database, must create
       userVotes.insert({
          userID: Meteor.userId(),
          urlID: currentURLID,
          upvoted: 0,
          downvoted: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        });
       Votes.update(
          { _id: currentURLID },
          { $inc: { downvote: 1} }
        );
       return 1;//turn color on
    }
  },
  commentUpvote: function(commentID, postID) {

    // console.log("current: "+(new Date()));
    // console.log("date: "+dateHoursAgo);
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(userCommentsVotes.findOne({userID: Meteor.userId(), comment_id: commentID})) {
      var userVoteInfo = userCommentsVotes.findOne({userID: Meteor.userId(), comment_id: commentID});

      var secondsToGoBack = 3;
      var dateSecondsAgo = new Date();
      var daysToArchive = new Date();
      dateSecondsAgo.setSeconds(dateSecondsAgo.getSeconds()-secondsToGoBack);
      daysToArchive.setHours(daysToArchive.getHours()-archiveHoursAmount);
      var dateUpdated = new Date(userVoteInfo.updatedAt);
      var dateCreatedAt = new Date(Comments.findOne({_id: commentID}).createdAt);
      //console.log("current: "+(new Date()));
      // console.log("updated: "+dateUpdated);
      // console.log("dateSec: "+dateSecondsAgo);
      // console.log("one day ago: "+daysToArchive);
      // console.log("created: "+dateCreatedAt);
      if(dateCreatedAt.getTime() < daysToArchive.getTime()) {
        throw new Meteor.Error("Error: This post has been archived.");
      }
      if(dateUpdated.getTime() > dateSecondsAgo.getTime()) {
        //console.log("inside: ");
        var dif = dateSecondsAgo.getTime() - dateUpdated.getTime();
        var secondsLeft = Math.round(dif / 1000, 2);
        secondsLeft = Math.abs(secondsLeft);
        throw new Meteor.Error("Error: You must wait '"+secondsLeft+"' more seconds to vote.");
      }
      if(userVoteInfo.upvoted) {
       // throw new Meteor.Error("Error: Allready upvoted.");
       //console.log("allready upvoted");
        userCommentsVotes.update(
         { userID: Meteor.userId(), comment_id: commentID },
         { $set: { upvoted: 0, downvoted: 0, updatedAt: new Date()} }
        );
        Comments.update(
          { _id: commentID },
          { $inc: { upvote: -1} }
        );
        return 0;//turn color off
      } else if(userVoteInfo.downvoted) {
        //console.log("allready downvoted");
        userCommentsVotes.update(
         { userID: Meteor.userId(), comment_id: commentID },
         { $set: { upvoted: 1, downvoted: 0, updatedAt: new Date()} }
        );
        Comments.update(
          { _id: commentID },
          { $inc: { downvote: -1, upvote: 1} }
        );
        return 1;//turn color on
      } else if(!userVoteInfo.downvoted && !userVoteInfo.upvoted) {
        //console.log("not down or up");
        userCommentsVotes.update(
         { userID: Meteor.userId(), comment_id: commentID },
         { $set: { upvoted: 1, downvoted: 0, updatedAt: new Date()} }
        );
        Comments.update(
          { _id: commentID },
          { $inc: { upvote: 1} }
        );
        return 1;//turn color on
      } else {
        throw new Meteor.Error("Error: Something wrong with user voting.");
      }
    } else {
      //no userVotes in database, must create
       userCommentsVotes.insert({
          userID: Meteor.userId(),
          comment_id: commentID,
          post_id: postID,
          upvoted: 1,
          downvoted: 0,
          createdAt: new Date(),
          updatedAt: new Date()
        });
       Comments.update(
          { _id: commentID },
          { $inc: { upvote: 1} }
        );
       return 1;//turn color on
    }
  },
  commentDownvote: function(commentID, postID) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(userCommentsVotes.findOne({userID: Meteor.userId(), comment_id: commentID})) {
      var userVoteInfo = userCommentsVotes.findOne({userID: Meteor.userId(), comment_id: commentID});
      //console.log(userVoteInfo);
       var secondsToGoBack = 3;
      var dateSecondsAgo = new Date();
      var daysToArchive = new Date();
      dateSecondsAgo.setSeconds(dateSecondsAgo.getSeconds()-secondsToGoBack);
      daysToArchive.setHours(daysToArchive.getHours()-archiveHoursAmount);
      var dateUpdated = new Date(userVoteInfo.updatedAt);
      var dateCreatedAt = new Date(Comments.findOne({_id: commentID}).createdAt);
      // console.log("current: "+(new Date()));
      // console.log("updated: "+dateUpdated);
      // console.log("dateSec: "+dateSecondsAgo);
      if(dateCreatedAt.getTime() < daysToArchive.getTime()) {
        throw new Meteor.Error("Error: This post has been archived.");
      }
      if(dateUpdated.getTime() > dateSecondsAgo.getTime()) {
        //console.log("inside: ");
        var dif = dateSecondsAgo.getTime() - dateUpdated.getTime();
        var secondsLeft = Math.round(dif / 1000, 2);
        secondsLeft = Math.abs(secondsLeft);
        throw new Meteor.Error("Error: You must wait '"+secondsLeft+"' more seconds to vote.");
      }
      if(userVoteInfo.downvoted) {
        console.log("allready downvoted");
       // throw new Meteor.Error("Error: Allready downvoted.");
        userCommentsVotes.update(
         { userID: Meteor.userId(), comment_id: commentID },
         { $set: { upvoted: 0, downvoted: 0, updatedAt: new Date()} }
        );
        Comments.update(
          { _id: commentID },
          { $inc: { downvote: -1} }
        );
        return 0;//turn color off
      } else if(userVoteInfo.upvoted) {
        console.log("allready upvoted");
        userCommentsVotes.update(
         { userID: Meteor.userId(), comment_id: commentID },
         { $set: { upvoted: 0, downvoted: 1, updatedAt: new Date()} }
        );
        Comments.update(
          { _id: commentID },
          { $inc: { downvote: 1, upvote: -1} }
        );
        return 1;//turn color on
      } else if(!userVoteInfo.downvoted && !userVoteInfo.upvoted) {
        console.log("not down or up");
        userCommentsVotes.update(
         { userID: Meteor.userId(), comment_id: commentID },
         { $set: { upvoted: 0, downvoted: 1, updatedAt: new Date()} }
        );
        Comments.update(
          { _id: commentID },
          { $inc: { downvote: 1} }
        );
        return 1;//turn color on
      } else {
        throw new Meteor.Error("Error: Something wrong with user voting.");
      }
    } else {
      //no userVotes in database, must create
       userCommentsVotes.insert({
          userID: Meteor.userId(),
          comment_id: commentID,
          post_id: postID,
          upvoted: 0,
          downvoted: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        });
       Comments.update(
          { _id: commentID },
          { $inc: { downvote: 1} }
        );
       return 1;//turn color on
    }
  },
  deleteComment: function(commentID) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(Meteor.userId() != Comments.findOne({ "auth.id": Meteor.userId(), _id: commentID}).auth.id) {
      throw new Meteor.Error("Error: You cannot delete this comment since you are not the author.");
    }
    Comments.update(
       { _id: commentID },
       { $set: { deletedAt: new Date()} }
     );
  },
  goLive: function() {
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    if(!liveChannelPlaylistLocation.findOne({user_id: Meteor.userId()})) {
      var endingDate = new Date();
      endingDate.setDate(endingDate.getDate() + 9000);
      liveChannelPlaylistLocation.insert({
        user_id: Meteor.userId(),
        createdAt: new Date(),
        updatedAt: new Date(),
        endingAt: endingDate,
        currentLocation: 0
      });
    } else {
      //user has already been live before
      var endingDate = new Date();
      endingDate.setDate(endingDate.getDate() + 9000);
      liveChannelPlaylistLocation.update(
        { user_id: Meteor.userId() },
        { $set: { updatedAt: new Date(), endingAt: endingDate, currentLocation: 0  } }
      );
    }
  },
  addLiveChannelURL: function(url, channel) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("Error: You must log in to use this feature!");
    }
    var channelUser_id = Meteor.users.findOne({username: channel})._id;
    var playlistLocationData = liveChannelPlaylistLocation.findOne({user_id: channelUser_id});

    var currentDate = new Date();
    var updatedAtDate = new Date(playlistLocationData.updatedAt);
    var endingDate = new Date(playlistLocationData.endingAt);
    if(currentDate > updatedAtDate && currentDate < endingDate) {
      //currently accepting videos (live)
      var validYoutubeRegex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
      if(url.match(validYoutubeRegex)) {
        var youtubeID = RegExp.$1;
      } else {
         throw new Meteor.Error("Error: Not a valid Youtube URL");
      }
      var hoursToGoBack = 3;//3 hours users must wait to submit the same video
      var dateHoursAgo = new Date();
      dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
      if(liveChannelPlaylists.findOne({ createdAt: { $gte: updatedAtDate }, youtubeID: { $eq: youtubeID } })) {
           throw new Meteor.Error("Error: This video has been submitted recently.");
      }
      liveChannelPlaylists.insert({
        channel_id: channelUser_id,
        channel_username: channel,
        createdAt: currentDate,
        youtubeID: youtubeID,
        submitter_id: Meteor.userId(),
        submitter_username: Meteor.user().username,
        deletedAt: null,
        upvotes: 0,
        downvotes: 0
      });
    } else {
      throw new Meteor.Error("Error: This channel is not currently live!");
    }
  }
});
