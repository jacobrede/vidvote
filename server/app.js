if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
  Meteor.publish("userSettings", function () {
     return userSettings.find({ userID: this.userId});
  });
  Meteor.publish("userVotes", function(hoursToGoBack) {
     var dateHoursAgo = new Date();
     dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
     return userVotes.find(
        { userID: this.userId, createdAt: { $gte : dateHoursAgo } }
        );
  });
  Meteor.publish("hotVotes", function (inSkip, inLimit, hoursToGoBack) {
      var dateHoursAgo = new Date();
      dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
      return Votes.find(
        { createdAt: { $gte : dateHoursAgo }, deletedAt: null },
        { sort: { weight: -1 } },
        { skip: inSkip, limit: inLimit }
        );
  });
  Meteor.publish("newVotes", function (inSkip, inLimit, hoursToGoBack) {
      var dateHoursAgo = new Date();
      dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
      return Votes.find(
        { createdAt: { $gte : dateHoursAgo }, deletedAt: null },
        { sort: { createdAt: -1 } },
        { skip: inSkip, limit: inLimit }
        );
  });
  Meteor.publish("topVotes", function (inSkip, inLimit, hoursToGoBack) {
    //console.log("server hoursToGoBack: "+hoursToGoBack);
      var dateHoursAgo = new Date();
      dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
      return Votes.find(
        { createdAt: { $gte : dateHoursAgo }, deletedAt: null },
        { sort: { upvote: -1, downvote: 1 } },
        { skip: inSkip, limit: inLimit }
        );
  });
  Meteor.publish("singleVotes", function (urlID) {
      return Votes.find(
          { _id: urlID, deletedAt: null }
        );
  });
  Meteor.publish("singleUserVotes", function(urlId) {
     return userVotes.find(
          { userID: this.userId, urlID: urlId }
        );
  });
  Meteor.publish("singleComments", function(urlId) {
     return Comments.find(
          { post_id: urlId }
        );
  });
  Meteor.publish("singleUserCommentsVotes", function(urlId) {
     return userCommentsVotes.find(
          { post_id: urlId }
        );
  });
  Meteor.publish("liveChannelPlaylists", function(channelUserName){
    var channelUser_id = Meteor.users.findOne({username: channelUserName})._id;
    var userLocationData = liveChannelPlaylistLocation.findOne({user_id: channelUser_id });
    var dateUpdated = new Date(userLocationData.updatedAt);
    var dateCreated = new Date(userLocationData.updatedAt);
    var dateEnd = new Date(userLocationData.endingAt);
    if(dateUpdated != dateCreated) {
      return liveChannelPlaylists.find(
           { channel_id: channelUser_id, createdAt: { $gte : dateUpdated, $lt : dateEnd }, deletedAt: null  },
           { sort: { createdAt: 1 } }
         );
    } else {
      return liveChannelPlaylists.find(
           { channel_id: channelUser_id, createdAt: { $gte : dateCreated, $lt : dateEnd  }, deletedAt: null  },
           { sort: { createdAt: 1 } }
         );
    }
  });
  Meteor.publish("liveChannelPlaylistLocation", function(channelUserName) {
    var channelUser_id = Meteor.users.findOne({username: channelUserName})._id;
    return liveChannelPlaylistLocation.find({user_id: channelUser_id });
  });
  function log10(val) {
    return Math.log(val) / Math.LN10;
  }
  var setUserTotalUpvotes = new Cron(function()  {
  //  console.log("setUserTotalUpvotes inside");
    Meteor.users.find().forEach(function(user) {
    //  console.log("inside");
      var totalUpvotes = 0;
      var totalDownvotes = 0;

      Votes.find(
        { userId: user._id },
        { fields: { "upvote":1, "downvote":1 } }
      ).forEach(function(data) {
        //console.log(data);
    //    console.log("inside foreach");
        totalUpvotes += data.upvote;
        totalDownvotes += data.downvote;
      });
      var totalNet = (totalUpvotes-totalDownvotes);
      Meteor.users.update({_id: user._id}, {$set: { "postUpvotes": totalUpvotes, "postDownvotes": totalDownvotes, "postNetVotes": totalNet  } });
    //  console.log("finsih");
    });
    return;
  }, {
    minute: 0//on every hour
  });
  var getWeight = function(ups, downs, dateTime) {
      var postsDate = new Date(dateTime);
      var score = ups - downs;
      var sign = 0;
      order = log10(Math.max(Math.abs(score), 1));
      if(score < 0) {
        sign = -1;
      } else if(score > 0) {
        sign = 1;
      }
      var seconds = postsDate.getTime() - 1441671235411;
      seconds = (seconds/1000)%60;
     // console.log(+((sign * order) + seconds / 45000).toFixed(2));
      return +(sign * order + seconds / 45000).toFixed(2);//+ is for an int, not a string
    }
  var updateWeight = new Cron(function() {
    var hoursToGoBack = 24;//keep this hard coded so were only updating weights of posts < 1 day
    var dateHoursAgo = new Date();
    dateHoursAgo.setHours(dateHoursAgo.getHours()-hoursToGoBack);
      var Posts = Votes.find({
        createdAt: {
          $gte : dateHoursAgo
        }
      }).forEach(function(data) {
        //console.log(data._id);
        Votes.update(
          {_id: data._id},
          {
            $set: {
              weight: getWeight(data.upvote, data.downvote, data.createdAt)
            }
          });

      });
    //  console.log("updateWeight finish");
    }, {
      minute: 0//on every hour
    });
  Accounts.onCreateUser(function(options,user) {
    if(!userSettings.findOne({userID: user._id})) {
        userSettings.insert({
          userID: user._id,
          autoPlay: 0,
          autoNext: 0
        });
    }
     if (options.profile)
        user.profile = options.profile;
      return user;
  });

}
